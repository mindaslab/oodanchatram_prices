import csv
import requests
from bs4 import BeautifulSoup
import pandas as pd
from datetime import date
import os




def oddanchatram_scrape_data(date,folder_path):

    url = "https://oddanchatramvegetablemarket.net/oddanchatram-vegetable-market-price-details-{}/".format(date)

    response = requests.get(url, timeout=10)

    if (response.status_code == 404):
        
        print("Oops!, There is no data in the clouds for the date {}.".format(date))

    elif (response.status_code == 200):

        soup = BeautifulSoup(response.content, 'html.parser')

        table = soup.find('table')

        rows = table.select('tbody > tr')

        suffix = '_{}.csv'.format(date)

        base_filename = "oddanchatram_price"

        file_name = os.path.join(folder_path, base_filename + suffix)


        with open(file_name, 'w', encoding="utf-8") as csv_file:
            writer = csv.writer(csv_file)
            for row in rows:
                data = [td.text.strip('\n\xa0\xa0 \xa0 \xa0') for td in row.find_all('td')]
                writer.writerow(data)
                print(data)

    else:
        print("Oops! Request Failed.")


if __name__=="__main__":

    start_date = input("Enter the start date in the mm-dd-yyyy format: ")
    end_date = input("Enter the end date in the mm-dd-yyyy format: ")
    folder_path = input("Enter the path where you want to save the file in your local system: ")
    daterange = pd.date_range(start_date, end_date)
    for raw_date in daterange:
        date = raw_date.strftime('%d-%m-%Y')
        oddanchatram_scrape_data(date,folder_path)

#date: 09-02-2021
#location: C:\Users\Saravanan\Documents\Karthi Scraping outputs (your path)

